twms (0.07z+git20201202+bb7c3f8-1) unstable; urgency=medium

  * New upstream release.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 02 Dec 2020 14:48:22 +0100

twms (0.07z+git20201130+6edd37d-2) unstable; urgency=medium

  * Bump debhelper from old 12 to 13.
  * Replace spaces in short license names with dashes.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Use canonical URL in Vcs-Git.

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 30 Nov 2020 14:24:29 +0100

twms (0.07z+git20201130+6edd37d-1) unstable; urgency=medium

  * New upstream snapshot (Closes: #973098).

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 30 Nov 2020 14:22:53 +0100

twms (0.07z+git20200829+cb7d39a-1) unstable; urgency=medium

  * New upstream snapshot.
  * Watch the upstream Git.
  * Drop no longer relevant patch (Closes: #965951).
  * Switch to dh 12 and stop using dh-systemd (Closes: #958601).
  * Promote cairo and pyproj to dependencies.

 -- Andrej Shadura <andrewsh@debian.org>  Sat, 29 Aug 2020 17:42:11 +0200

twms (0.07z-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Deprecating priority extra as per policy 4.0.1

  [ Andrej Shadura ]
  * New upstream release (Closes: #915933, #916305).
  * Update debian/watch.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 06 Jan 2019 19:53:47 +0100

twms (0.06y-2) unstable; urgency=medium

  * Temporarily work around the encoding issue by forcefully setting
    LC_ALL to C.UTF-8.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 23 May 2018 10:56:42 +0100

twms (0.06y-1) unstable; urgency=medium

  * New upstream release (Closes: #866489).
  * Refresh the patches.
  * Switch to Python 3.
  * Update debian/copyright.
  * Update the homepage.
  * Update Vcs-*.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 13 May 2018 09:24:01 +0200

twms (0.05t-4) unstable; urgency=medium

  * Build-Depend on dh-python.

 -- Andrew Shadura <andrewsh@debian.org>  Tue, 05 Jul 2016 16:23:59 +0200

twms (0.05t-3) unstable; urgency=medium

  * Add systemd unit file.

 -- Andrew Shadura <andrewsh@debian.org>  Tue, 05 Jul 2016 12:18:27 +0200

twms (0.05t-2) unstable; urgency=low

  * Add OpenRC support.
  * Remove RUN parameter support from /etc/default/twms.

 -- Andrew Shadura <andrewsh@debian.org>  Wed, 15 Jan 2014 03:07:07 +0100

twms (0.05t-1) unstable; urgency=low

  * New upstream release.
  * Use dh_python2 instead of dh_pysupport.
  * Bump compat level to 9.
  * Update Standards-Version to 3.9.4.
  * Update init script and the manpage.

 -- Andrew Shadura <andrewsh@debian.org>  Sat, 10 Aug 2013 20:51:29 +0200

twms (0.04r-3) unstable; urgency=low

  * Upload to unstable.
  * Install manpages into a proper place.

 -- Andrew Shadura <andrewsh@debian.org>  Sat, 10 Aug 2013 16:49:40 +0200

twms (0.04r-2) experimental; urgency=low

  * Apply upstream patch to handle bz2-compressed GPX tracks.
  * Recommend python-cairo.

 -- Andrew Shadura <andrewsh@debian.org>  Mon, 08 Apr 2013 10:21:52 +0200

twms (0.04r-1) experimental; urgency=low

  * New upstream release.
  * Drop patches applied upstream.
  * Update copyright file: upstream is now WTFPL.
  * Upstream now has a Makefile, simplify rules.
  * Update dependencies:
    - Remove python-psyco.
    - Move python-pyproj to Recommends.
  * Update Vcs-* fields.

 -- Andrew Shadura <andrewsh@debian.org>  Tue, 19 Mar 2013 16:06:07 +0100

twms (0.03e-2) unstable; urgency=low

  * Fix incorrect error handing that resulted in internal server errors.
  * Accept WMS query parameters in mixed case (fixes interoperability
    problems with JOSM).
  * Don't forcibly chown cache directory in postinst.
  * Fix FTBFS with funny umask set (Closes: #674817).
  * During package build, fix file permissions inside the build directory,
    not on the source files.
  * Bump Standards-Version to 3.9.3, no changes.

 -- Andrew O. Shadura <bugzilla@tut.by>  Mon, 28 May 2012 19:18:25 +0200

twms (0.03e-1) unstable; urgency=low

  * New upstream version released.
  * Update the description.
  * Bump Standards-Version.
  * Depend on adduser as we use it to add a system user to run the daemon as.
  * Update the spelling of the maintainer's last name.
  * Update the dependency on webpy.
  * Check the default Python version when installing (patch by Jakub Wilk).
  * Suggest python-psyco instead of recommending it (see #614619).
  * Provide python-twms package.
  * Make it possible to configure port number.
  * Update the manual page.
  * Fix the configuration file example so the URLs have the correct port
    number.
  * Rewrite the init script.

 -- Andrew O. Shadura <bugzilla@tut.by>  Tue, 06 Dec 2011 11:30:32 +0100

twms (0.02w-1) unstable; urgency=low

  * Initial release (Closes: #587522)

 -- Andrew O. Shadura <bugzilla@tut.by>  Fri, 13 Aug 2010 17:37:27 +0300
